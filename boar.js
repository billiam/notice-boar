var noticeBoar = {

  attachNoticeBoar: function() {
    $("div#main div#primary_container div.pbr_dashboard div.column div.portlet_section.notice_schedule").each(function(index) {
      var header = $(this).find("div.portlet_section_header h2");
      if (header.length > 0 && $(header[0]).text().indexOf("Notice Board") > -1) {
        $(header[0]).text("Notice Boar");

        var max = 7;
        var min = 0;
        var picNumber = Math.floor(Math.random() * (max - min + 1)) + min;
        var boarUrl = chrome.extension.getURL("/images/notice_boar_" + picNumber + ".png");
        $(this).find("div.portlet_section_content").prepend('<img src="' + boarUrl + '" style="width: 100%; padding-bottom: 10px;" />');
      }
    }); 
  }
};

noticeBoar.attachNoticeBoar();